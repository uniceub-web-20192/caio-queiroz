package main

import ( "net/http"
		 "time" 
		 "log")

func main() {

	StartServer()
	log.Println("[INFO] Servidor no ar!")
}

func requests(w http.ResponseWriter, r *http.Request) {

	m    := r.Method
	path := r.URL.String()
	

	w.Write([]byte("method: "  + m + "   path: " + path ))
	
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.32:8082",
			IdleTimeout: duration, 
	}

	http.HandleFunc("/", requests)

	log.Print(server.ListenAndServe())
}